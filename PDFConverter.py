import sys #default import, not actually being used by this program, could be used for debug
from reportlab.lib import colors #reportlab imports are for tables and table formatting
from reportlab.lib.pagesizes import letter, inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from PyPDF2 import PdfFileWriter, PdfFileReader #PyPDF2 allows reading and writing of pdfs
from reportlab.platypus.flowables import Image #reportlab import which allows image handling
import json #JavaScript Object Notation import
from pprint import pprint #console printing import for debug
from reportlab.platypus import Indenter #allows indentation of elements
from reportlab.lib.colors import HexColor #allows for custom coloring


############################
#     CONSTANTS            #
############################

CONST_FILEPATHJSONFROM = 'JSONData' #filepath to read JSON data from

CONST_FILEPATHPDFWRITE = 'simple_table_grid.pdf' #filepath to write pdf to

CONST_ROWBACKGROUNDOFFSETCOLOR = HexColor('#f4f7fc') #background color for the offset rows in the main and sum tables

#sizing consts for the main table, necessary because table does not accept expressions as widths
CONST_SMALLCOL = 0.3*inch
CONST_MEDCOL = 0.35*inch

#Use the following to size the 27 columns in the main table, be sure that the colwidths element contains 27 values,
#or you will receive a compilation error
#currently the first 22 are the same (small), last 5 are the same (med)
colwidths = (CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,
  CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,
  CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,
  CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,CONST_SMALLCOL,
  CONST_MEDCOL, CONST_MEDCOL, CONST_MEDCOL, CONST_MEDCOL, CONST_MEDCOL,)


#################################################################################
# WARNING - DO NOT CHANGE THESE UNLESS YOU KNOW WHAT YOU ARE DOING              #
# changing these values without changing the data going into the header file    #
# will cause a compilation error                                                #
CONST_HEADERTABLECOLS = 11                                                      #
CONST_HEADERTABLEROWS = 8                                                       #
#                                                                               #
#    These are included as a nicety for future development mostly               #
#################################################################################




'''''''''''''''''''''''''''''
Getting JSON Data
'''''''''''''''''''''''''''''
#getting data as JSON object from a file
with open(CONST_FILEPATHJSONFROM) as data_file:    
    jsondata = json.load(data_file)

#changing how the script recieves the JSON data may be done here, just be 
#sure to keep the name of the json data as "jsondata", and keep it
#in an expected format

#pprint(jsondata) # debug


'''''''''''''''''''''''''''''
Setting up PDF doc
'''''''''''''''''''''''''''''
#PDF doc setup
doc = SimpleDocTemplate(CONST_FILEPATHPDFWRITE, pagesize=letter,  rightMargin=0,leftMargin=0,
                        topMargin=0,bottomMargin=0) #default doc object with no margins 

#container for the 'Flowable' objects, will hold the entire page, then append to the doc
elements = []


'''''''''''''''''''''''''''''
Header Tables & Logo
'''''''''''''''''''''''''''''

#adding the logo to the top of the page
headerlogo = Image("Form Header.jpg", 8.5*inch, 1.5*inch) #sizing the logo
elements.append(headerlogo) #appending logo to flowable


#creating the date and ticket table data
DTData= [["Date : " + str(jsondata['date']),"Ticket# : " + str(jsondata['ticketnum'])]]
DTTable = Table(DTData, 2*[2*inch],1*[0.3*inch], hAlign='LEFT')#creating the table itself


#appending DTTable to the flowable
elements.append(Indenter(left=0.3*inch)) #adds indentation
elements.append(DTTable)
elements.append(Indenter(left=-0.3*inch)) #removes indentation for future elements


#creating the header table object with default labels, 
#the table contains lots of specific padding in the form of styling as well as empty elements
#tabs show data elements as relative to column locations
HeaderData = [["Sale Name",   " : " + str(jsondata['salename']),    "","",  "Contract# ",     "", " : " + str(jsondata['contractnum']),   "", "",           "Contract Minimums",                      ""],
              ["Logger",      " : " + str(jsondata['logger']),      "","",  "Source Load#",   "", " : " + str(jsondata['sourceloadnum']), "", "Diameter",   " : ",                                    str(jsondata['contractmindia'])],
              ["Seller",      " : " + str(jsondata['seller']),      "","",  "Seller Load#",   "", " : " + str(jsondata['sellerloadnum']), "", "Length",     " : ",                                    str(jsondata['contractminlen'])],
              ["Buyer",       " : " + str(jsondata['buyer']),       "","",  "Buyer Load#",    "", " : " + str(jsondata['buyerloadnum']),  "", "",           "",                                       ""],
              ["Trucker",     " : " + str(jsondata['trucker']),     "","",  "Truck#",         "", " : " + str(jsondata['trucknum']),      "", "",           "Weight",                                 ""],
              ["Destination", " : " + str(jsondata['destination']), "","",  "Scaler Code",    "", " : " + str(jsondata['scalercode']),    "", "",           "Gross: " + str(jsondata['weightgross']), ""],
              ["Deck",        " : " + str(jsondata['deck']),        "","",  "Sale Hammer",    "", " : " + str(jsondata['salehammer']),    "", "",           "Tare: " + str(jsondata['weighttare']),   ""],
              ["Scale Type",  " : " + str(jsondata['scaletype']),   "","",  "",               "", "",                                     "", "",           "Net: " + str(jsondata['weightnet']),     ""]  
              ]


#defining the header table object as a reportlab table
HeaderTable = Table(HeaderData,CONST_HEADERTABLECOLS*[0.7*inch],CONST_HEADERTABLEROWS*[0.3*inch], spaceBefore=12, spaceAfter=12, hAlign='LEFT')

#defining the style for the reportlab table object
HeaderTable.setStyle(TableStyle([('ALIGN',(0,0), (-1,-1),'LEFT'),
                        ('VALIGN',(0,0), (-1,-1),'BOTTOM'),
                        ('ALIGN',(9,0),(9,-1),'CENTER'), #align row 9 elements center, these are 'contract minimum', 'weights' and elements below 'weights'
                        ('ALIGN',(10,0),(10,-1),'LEFT'), #align "diameter" and "length" values
                        ('ALIGN',(8,1),(8,-1),'RIGHT'), #align "diameter" and "length" labels
                        ('LINEBELOW',(8,0),(10,0), 0.25, colors.black), #line for "contract minimum"
                        ('LINEBELOW',(8,4),(10,4), 0.25, colors.black), #line for "weight"
                        ('SIZE',(0,0), (-1,-1),9.5) #font size for header table
                       ]))

#appending the table to the final flowable obj
elements.append(Indenter(left=0.3*inch)) #adds indentation
elements.append(HeaderTable)
elements.append(Indenter(left=-0.3*inch)) #removes indentation for future elements

'''''''''''''''''''''''''''''
Main Table
'''''''''''''''''''''''''''''

#getting data from the JSON obj
TableData = jsondata['logs']
lognum = len(jsondata['logs'])

#Inserting the 2nd to top row of data, the labels, explicitly
TableData.insert(0, [
      "Log#", "LG","D1","D2","SP","SSL","DL","DD","%D","FT","GD","DL","DD","%D","FT",
      "GD","DL", "DD", "%D", "FT", "GD", "MSM", "GRS", "NET", "SSL", "UC", "SC"
      ])

lognum = lognum + 1 #increment the row counter to include this addition

#Inserting the flying row with segment labels and empty elements
TableData.insert(0, [
      "",  "", "", "", "","","", "",
      "SEGMENT ONE",
      "","","","",
      "SEGMENT TWO",
      "", "", "", "",
      "SEGMENT THREE",
      "","","","","","",
      "ADJ GROSS",""
      ])

lognum = lognum + 1 #increment the row counter to include this addition

#pprint(TableData) #debug


#sizing the main data table, initiating with dynamic rows and constant columns      
MainTable=Table(TableData, colwidths, lognum*[0.25*inch], spaceBefore=12, spaceAfter=12, hAlign='CENTER')

#styling for the main table
MainTable.setStyle(TableStyle([('ALIGN',(0,0), (-1,-1),'LEFT'), #aligns all cells to the left
                        ('ALIGN',(0,0), (25,1),'CENTER'), #aligns top row as center
						            ('SIZE',(0,0), (-1,-1),6.5), #sets the text size for the table
                        ('VALIGN',(0,0), (-1,-1),'BOTTOM'),

                        ('INNERGRID', (0,1),(26,1),0.25, colors.black),
                        ('INNERGRID', (0,2), (-1,-1), 0.25, colors.white),
                        ('BOX', (0,1), (-1,-1), 0.2, colors.black),

                        ('LINEABOVE', (6,0),(20,0), 0.25, colors.black), #top line for the three floating segments
                        ('LINEBEFORE', (6,0),(6,-1), 0.25, colors.black), #left outer line ^
                        ('LINEAFTER', (20,0),(20,-1), 0.25, colors.black), #right outer line ^
                        ('LINEBEFORE', (11,0),(11,-1), 0.25, colors.black), #left inner line ^
                        ('LINEBEFORE', (16,0),(16,-1), 0.25, colors.black), #right inner line ^

                        ('LINEABOVE', (25,0),(26,0), 0.25, colors.black), #top line for adj gross floating segment
                        ('LINEAFTER', (26,0),(26,0), 0.25, colors.black), #right outer line ^
                        ('LINEBEFORE', (25,0),(25,0), 0.25, colors.black), #left outer line ^
                        ('ALIGN',(25,0), (26,0),'LEFT'), #aligning the "adj gross" text to fit in the box

                        ('LINEBELOW', (0,1), (-1,1), 0.25, colors.black), #label row (2nd row) underline

                        ('ROWBACKGROUNDS', (0, 2), (-1, -1), [ #repeating, built in color changes on the rows
                        CONST_ROWBACKGROUNDOFFSETCOLOR, colors.white #light blue-grey and white
                        ]),
                        ]))

#add Main Table to the flowable obj, no indentation
elements.append(MainTable)


'''''''''''''''''''''''''''''
Summary Table
'''''''''''''''''''''''''''''

#getting the summary table info from the JSON obj
TotData = jsondata['tots']
totnum = len(jsondata['tots'])

TotData.insert(0, [
    "Species","Quant.","Segs.","Gross","Net",
    "SSL","UC","SC"
    ])

totnum = totnum + 1 #increment the row counter to include this addition

TotData.insert(0,[
    "","","","","","","ADJ GROSS",""
])

totnum = totnum + 1 #increment the row counter to include this addition

TotData.append([
    "Totals", "x","x+1","2x","x^2","x^x","x/2","x/x^2"
  ])
totnum = totnum + 1 #increment the row counter to include this addition

#creating sum table
SummaryTable=Table(TotData,8*[0.4*inch], totnum*[0.25*inch], spaceBefore=12, spaceAfter=12, hAlign='LEFT')

#styling sum table
SummaryTable.setStyle(TableStyle([('ALIGN',(0,0), (-1,-1),'LEFT'),
                        ('SIZE',(0,0), (-1,-1),6.5), #font
                        ('VALIGN',(0,0), (-1,-1),'BOTTOM'),

                        ('ROWBACKGROUNDS', (0, 2), (-1, -1), [ #alternating colored backgrounds is luckily built in
                        CONST_ROWBACKGROUNDOFFSETCOLOR, colors.white
                        ]),

                        ('INNERGRID', (0,2), (-1,-1), 0.25, colors.white),

                        ('LINEABOVE', (6,0),(8,0), 0.25, colors.black), #top line for adj gross floating segment
                        ('LINEAFTER', (7,0),(7,0), 0.25, colors.black), #right outer line ^
                        ('LINEBEFORE', (6,0),(6,0), 0.25, colors.black), #left outer line ^

                        ('BOX', (0,1), (-1,-1), 0.25, colors.black),

                        ('LINEABOVE', (0,-1),(-1,-1), 0.25, colors.black), #line above bottom floating total
                        ('BACKGROUND', (0,-1),(-1,-1), colors.white), #designating background as always white for bottom row
                        ('INNERGRID', (0,-1),(-1,-1), 0.25, colors.black), #inner grid for final row

                       ]))

#adding the summary table to the flowable object
elements.append(Indenter(left=0.2*inch)) #adds indentation
elements.append(SummaryTable)
elements.append(Indenter(left=-0.2*inch)) #removes indentatino for future objects


# write the document to file
doc.build(elements)